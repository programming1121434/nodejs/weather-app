const fetchAPIKey = async () => {
  try {
    const response = await fetch('/api/config'); // Assuming your Express server is running on the same host and port
    const data = await response.json();
    const API_KEY = data.API_KEY;
    return API_KEY;
  } catch (error) {
    console.error('Error fetching API_KEY:', error);
    return null;
  }
};


const makeIconURL = (iconId) =>
  `https://openweathermap.org/img/wn/${iconId}@2x.png`;

const getFormattedWeatherData = async (city, units = "metric") => {
  const API_KEY = await fetchAPIKey(); // Get the API_KEY from the API endpoint
  if (!API_KEY) {
    return null;
  }

  const URL = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=${units}`;

  const data = await fetch(URL)
    .then((res) => res.json())
    .then((data) => data);

  const {
    weather,
    main: { temp, feels_like, temp_min, temp_max, pressure, humidity },
    wind: { speed },
    sys: { country },
    name,
  } = data;

  const { description, icon } = weather[0];

  return {
    description,
    iconURL: makeIconURL(icon),
    temp,
    feels_like,
    temp_min,
    temp_max,
    pressure,
    humidity,
    speed,
    country,
    name,
  };
};

export { getFormattedWeatherData };
