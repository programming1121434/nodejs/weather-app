FROM node:18.14.1 as builder
WORKDIR /app
RUN mkdir server
COPY server/ /app/server
COPY package*.json /
RUN npm install
COPY . .
RUN npm run build
# WORKDIR /app/server
# RUN npm install
# CMD ["node", "app.js"]
CMD ["npm", "run", "start-server"]

# FROM nginx:1.21
# RUN rm -rf /usr/share/nginx/html/*
# COPY --from=builder /app/server/build /usr/share/nginx/html
# # COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
# EXPOSE 80
# CMD ["nginx", "-g", "daemon off;"]