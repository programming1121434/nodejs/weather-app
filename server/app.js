const express = require('express')
const app = express()

const API_KEY = process.env.REACT_APP_API_KEY
console.log(API_KEY)

const PORT = process.env.PORT || 80;

app.listen(PORT, () => console.log('Server Started'));

app.use(express.static("../build"))

app.get("/", (req, res) => {
    res.sendFile('../build', 'index.html')
})

app.get("/api/config", (req, res) => {
    res.json({ API_KEY });
});

module.exports = app;

